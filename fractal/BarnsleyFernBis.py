 # -*- coding: utf-8 -*-
from __future__ import print_function, absolute_import
import numpy as np
import matplotlib.pyplot as plt
import scipy.misc


class BarnsleyFern:
    def __init__(self,x,y):
        self.x = x
        self.y = y
        self.param = np.array([[0.    , 	0.       , 	0.       , 	0.16, 	0., 	0.   , 	0.01],
                              [0.85 , 	0.04    , 	-0.04   , 	0.85, 	0, 	1.60, 	0.85],
                              [0.20 , 	-0.26   ,	0.23    , 	0.22, 	0,	1.60, 	0.07],
                              [-0.15,	0.28    , 	0.26    , 	0.24, 	0, 	0.44, 	0.07]]) #a,b,c,d,e,f,p
        self.color= [50,255,50]#[random.randint(0,255),random.randint(0,255),random.randint(0,255)]
        self.proba = np.cumsum(self.param[:,6])

    def fx(self, ):
        r = np.random.uniform()
        if r < self.proba[0]:
            i=0
        elif self.proba[0] <= r < self.proba[1]:
            i=1
        elif self.proba[1] <= r < self.proba[2]:
            i=2
        else:
            i=3
        x= self.param[i,0]*self.x + self.param[i,1]*self.y + self.param[i,4]
        self.y = self.param[i,2]*self.x + self.param[i,3]*self.y + self.param[i,5]
        self.x=x


class Apply:
    def __init__(self):
        self.nbi=0
        self.height=1080
        self.width=1920
        self.nb_itermax = 100000
        self.x0 =0.
        self.y0 =0.
        imgmat = np.zeros((self.width,self.height,3), dtype=np.uint8)
        self.BF = BarnsleyFern(self.x0,self.y0)
        #self.BF.iter(self.nb_itermax,imgmat,self.width,self.height)
        # img = Image.fromarray(imgmat, 'RGB')
        self.F = plt.figure()
        self.Ax = plt.imshow( np.swapaxes(imgmat, 1, 0))
        self.F.show()
        plt.pause(0.1)
        self.a=0
        return

    def iter(self,n):
        x = np.zeros(n)
        y = np.zeros(n)
        for i in np.arange(n):
            self.BF.fx()
            x[i],y[i]=self.BF.x,self.BF.y
        return x,y

    def scale(self,x, y ,width,height):
        x-=x.min()
        y-=y.min()
        rx =(width-1)/ (x.max() - x.min())
        ry =(height-1)/ (y.max() - y.min())
        r=min(rx,ry)
        x = (x-((x.max() - x.min())/2.))*r +width/2.
        y = y *r
        return x, y

    def coloredimg(self,img,x,y):
        for i in range(x.shape[0]):
            img[int(np.floor(x[i])),int(np.floor(y[i])),:]=self.BF.color

    def update_BF(self, ):
        if self.a ==0:
            self.BF.param = np.array([[0.154937859904,0.780101529111,-0.908228220033,0.200824281329,-0.665565035834,-0.626556429196,0.904617757881],
                            [-0.44177737237,0.1072448477,0.126124453293,0.169701955101,0.183849748874,0.690840729141,0.0780184035874],
                            [0.963226174932,0.918021430794,0.120989456266,-0.564731219767,-0.151993460158,0.0451972855045,0.00750429907985],
                            [-0.888136269268,-0.223853127125,-0.426763006354,0.104115878011,0.565708679539,-0.186464470845,0.00985953945201]])
        elif self.a ==1:
            self.BF.param = np.array([[0.786474547784,0.465460862724,-0.762674907775,0.302448806621,-0.884289440742,-0.534347299664,0.461760258737],
                            [ -0.393608649659,0.672130157525,0.883462908468,0.13952372621,-0.829713671214,  -0.141560806665,0.460079197374],
                            [0.233072936449,-0.127196775306,0.136987483542,0.605040305401, 0.533109801652,-0.480397303164,0.0104591996721],
                            [0.537265974128,-0.0083757407884,0.281041334825,-0.44150186358,0.541541057688,0.00608769750892,0.0677013442173]])
        elif self.a ==2:
            self.BF.param = np.array([[-0.928993047172,0.729810949298,0.141547669869,0.508339525208,0.693584091,-0.298010060406,0.0253831825219],
                            [  0.269023554147,-0.288457310293,-0.282147198728,0.552316224096,-0.776522329248,0.306049924182,0.1172880925],
                            [  -0.386241376088,0.3156888609,0.0144331854667,0.051808758359,0.471742419841,-0.971594708275,0.223691739755],
                            [0.734848528315,-0.576827082478,0.145967483736,0.584452997269,0.597477376718,-0.241031643887,0.633636985224]])

        elif self.a ==3:
            self.BF.param = np.array([[-0.529752058097,-0.147575761695,0.843997928661,0.308084805206,0.878514963891,0.659555050349,0.145929912122],
                            [ 0.404381079269,-0.685278462431,0.870667144298,0.246555926695,0.353004974776,0.118411619332,0.823730478276],
                            [ -0.783812914362,0.460747679752,0.22244768011,-0.0612780483534,-0.435863923777,-0.323479436416,0.0273591462583],
        [0.75264713296,-0.033616206896,-0.591022011727,-0.869747578054,0.0595129626198,0.483642774626,0.00298046334376]])

        elif self.a ==4:
            self.BF.param = np.array([[-0.904058878028,-0.150733347414,-0.0755092028866,0.564794236569,-0.123142153946,-0.49049913082,0.233711110108],
                            [-0.0847353559515,-0.587186372055,0.251618309327,-0.329161728726,-0.620504441665,0.991909968004,0.745269832766],
                            [0.655732970181,-0.215082848492,0.52837631486,-0.633628192619,0.0875136295981,0.037418567384,0.00473620926091],
                            [0.187057361927,0.432819900216,0.262612708615,0.404743730097,-0.779239553921,0.0421206807669,0.016282847865]])
        elif self.a ==5:
            self.BF.param = np.array([[-0.40280386889,-0.887413842928,-0.68925693714,0.12480227473,-0.702187511479,0.956755314221,0.123279505124],
                            [-0.90191251816,-0.235879347811,-0.103637123112,0.750827947138,-0.580908378252,0.83021689043,0.844040659273],
                            [ 0.700180547928,0.772135517262,-0.582528550757,-0.191292790781,0.626526066043,-0.889053936011,0.00120642359864],
                            [-0.233362369114,-0.593098590819,0.899735177399,-0.199802825559,0.458069496363,-0.131435965081,0.0314734120038]])

        else:
            self.a=-2
        print(self.a)
        # self.color= [random.randint(0,255),random.randint(0,255),random.randint(0,255)]
        self.BF.proba = np.cumsum(self.BF.param[:,6])
        self.BF.x = self.x0
        self.BF.y = self.y0
        imgmat = np.zeros((self.width,self.height,3), dtype=np.uint8)
        x,y=self.iter(self.nb_itermax)
        x,y=self.scale(x, y ,self.width,self.height)
        self.coloredimg(imgmat,x,y)

        self.Ax.set_data(np.flip(np.swapaxes(imgmat, 1, 0),0))
        plt.pause(0.1)
        self.nbi+=1
        self.a +=1
        # scipy.misc.imsave('BF'+str(self.nbi).zfill(5)+'.png', np.flip(np.swapaxes(imgmat, 1, 0),0))
        return


root = Apply()
i=0
while root.a>=0:
    root.update_BF()

